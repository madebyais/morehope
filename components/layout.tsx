import Head from "next/head"
import { FC } from "react"
import NavBarMain from "./navbar-main"

type LogoType = "primary" | "secondary"

interface ILayoutProps {
  title?: string
  className?: string
  currentPage?: string
  logoType?: LogoType
}

const Layout: FC<ILayoutProps> = ({ title, className = "", currentPage = "home", logoType = "primary", children }) => {
  return (
    <div>
      <Head>
        <title>{title ? title : "More Hope Indonesia"}</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" />
      </Head>

      <main className={className}>
        <div className="container mx-auto py-5 flex justify-between items-center">
          <div>
            {logoType == "primary" ? (
              <img src="/images/logo-primary.png" width={150}/>
            ):(
              <img src="/images/logo-secondary.png" width={150}/>
            )}
          </div>
          <div><NavBarMain currentPage={currentPage} /></div>
        </div>

        {children}
      </main>
    </div>
  )
}

export default Layout