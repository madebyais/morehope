import { FC, useState } from "react";

interface INavBarMainProps {
  currentPage?: string
}

enum MenuItemType {
  ICON = "icon",
  TEXT = "text"
}

interface IMenuItem {
  type: MenuItemType,
  id: string,
  label: string,
  href: string
}

const NavBarMain: FC<INavBarMainProps> = ({ currentPage }) => {
  const menus: Array<IMenuItem> = [
    { type: MenuItemType.TEXT, id: "home", label: "HOME", href: "/" },
    { type: MenuItemType.TEXT, id: "about", label: "ABOUT MOREHOPE", href: "/about" },
    { type: MenuItemType.TEXT, id: "more-news", label: "MORE NEWS", href: "/more-news" },
    { type: MenuItemType.TEXT, id: "manifesto", label: "MANIFESTO", href: "/manifesto" },
    { type: MenuItemType.TEXT, id: "more-map", label: "MOREMAP", href: "/more-map" },
    { type: MenuItemType.TEXT, id: "shop", label: "SHOP", href: "/shop" }
  ]

  const [toggleMobileMenu, setToggleMobileMenu] = useState<string>('hidden')
  const onToggleMobileMenu = () => {
    setToggleMobileMenu(toggleMobileMenu == 'hidden' ? 'block' : 'hidden')
  }

  return (
    <div>
      <div className="hidden md:block md:space-x-3">
        {menus.map((item, i) => (
          <a key={i} href={item.href} className={`text-xs px-5 py-2 border border-black rounded bg-white hover:bg-gray-300 ${currentPage === item.id ? "font-bold bg-gray-300" : ""}`}>{item.label}</a>
        ))}
      </div>

      <div className="block cursor-pointer border rounded border-black bg-white px-3 py-1 my-2 mr-5 text-center md:hidden" onClick={onToggleMobileMenu}>
        <i className={`fa fa-${toggleMobileMenu == 'hidden' ? 'bars' : 'times'}`}></i>
      </div>
      <div className={`${toggleMobileMenu} flex flex-col space-y-2 pr-4 md:hidden`}>
        {menus.map((item, i) => (
          <a key={i} href={item.href} className={`text-right text-xs px-5 py-2 border border-black rounded bg-white hover:bg-gray-300 ${currentPage === item.id ? "font-bold bg-gray-300" : ""}`}>{item.label}</a>
        ))}
      </div>
    </div>
  )
}

export default NavBarMain