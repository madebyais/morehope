import "../public/css/base.css"
import "../public/css/style.min.css"
import type { AppProps } from "next/app"

function App({ Component, pageProps }: AppProps) {
  return <Component {...pageProps} />
}

export default App