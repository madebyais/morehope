import Layout from "components/layout";

export default function ({}) {
  return (
    <Layout title="About More Hope" currentPage="about" logoType="secondary" className="w-full h-screen">
      <div className="container mx-auto md:flex h-full">
        <div className="px-5 md:w-3/6 md:px-0 md:pr-10">
          <div className="space-y-5 mb-20">
            <h1 className="font-bold uppercase text-6xl border-black border-b pb-5">
              .From more<br/>
              to the Hope.
            </h1>

            <div>MOREHOPE is a brand and a platform to realize expectations, become processes and materialize.</div>
            <div>With MOREHOPE we will be somethting useful and useful for many people, MOREHOPE creates a positive community and grows together to make all dreams come true.</div>
            <div>With passion and integrity, let's start one step MOREHOPE to be a FOOTWEAR.</div>
          </div>
          <div className="space-y-5">
            <h1 className="font-bold uppercase text-6xl border-black border-b pb-5">
              .Career.
            </h1>

            <div>Membuka peluang untuk Visual Artist dan Penggiat Metaverse menjadi bagian dari MOREHOPE untuk membangun sebuah dunia dengan kreatifitas dan produktifitas.</div>
          </div>
        </div>
        <div className="md:w-3/6 flex justify-center items-end"><img className="h-full" src="/images/about.png" /></div>
      </div>
    </Layout>
  )
}