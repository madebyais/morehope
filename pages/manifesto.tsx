import Layout from "components/layout";

export default function ({}) {
  return (
    <Layout title="Manifesto" currentPage="manifesto" className="w-full h-screen bg-image-manifesto">
      <div className="container mx-auto px-5 md:px-0 md:flex">
        <div className="space-y-5 md:w-4/6">
          <h1 className="font-bold uppercase text-6xl border-black border-b pb-5">
            MANIFESTO.
          </h1>

          <div>MOREHOPE is a brand and a platform to realize expectations, become processes and materialize.</div>
        </div>
      </div>
    </Layout>
  )
}