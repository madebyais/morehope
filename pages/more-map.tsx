import Layout from "components/layout";
import { useState } from "react";

const moreMap = {
  1: { bgColorId: 1, imageSrc: "/images/map-value-detail.png", description: "" },
  2: { bgColorId: 2, imageSrc: "/images/map-community-detail.png", description: "" },
  3: { bgColorId: 1, imageSrc: "/images/map-morehope-world-detail.png", description: "" },
  4: { bgColorId: 3, imageSrc: "/images/map-morehope-spot-detail.png", description: "" },
  5: { bgColorId: 2, imageSrc: "/images/map-theprogram-detail.png", description: "" },
  6: { bgColorId: 2, imageSrc: "/images/map-theidea-detail.png", description: "" },
  7: { bgColorId: 3, imageSrc: "/images/map-metaverse-detail.png", description: "" }
}

export default function ({}) {
  const [isShowDetail, setIsShowDetail] = useState<boolean>(false)
  const [detailId, setDetailId] = useState<number>(1)

  const showDetail = (id) => {
    setDetailId(id)
    setIsShowDetail(true)
  }

  if (isShowDetail) {
    return (
      <Layout title="More Map" currentPage="moremap">
        <div className="container mx-auto px-5 md:px-0">
          <div className="text-center font-bold text-6xl uppercase">.MOREMAP.</div>
          <div className="text-center font-bold text-xl uppercase mb-10">
            We are guided by simple yet profound vision - Create a large project<br/>
            for the more that is built and owned by the community
          </div>

          <div className="border shadow-lg p-5 rounded-lg" style={{backgroundColor: generateBackgroundColor(moreMap[detailId].bgColorId)}}>
            <div className="cursor-pointer text-2xl font-bold text-right" onClick={() => setIsShowDetail(false)}>x</div>
            <div className="md:flex md:items-center">
              <div className="w-full md:w-2/6"><img src={moreMap[detailId].imageSrc} style={{maxHeight: 300}} /></div>
              <div className="w-full md:w-4/6 font-bold">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
              </div>
            </div>
            
          </div>
        </div>
      </Layout>
    )
  }

  return (
    <Layout title="More Map" currentPage="moremap">
      <div className="container mx-auto px-5 md:px-0 mb-10">
        <div className="text-left font-bold text-6xl uppercase">.MOREMAP.</div>
        <div className="text-left font-bold text-xl uppercase">
          We are guided by simple yet profound vision - Create a large project<br/>
          for the more that is built and owned by the community
        </div>
      </div>

      <div className="container mx-auto px-5 md:px-0 md:flex md:gap-3">
        <div className="w-full space-y-3 mb-3 md:w-3/12">
          <MapContainer id={1} bgColorId={1} onClick={showDetail} title={`Value`} imageSrc="/images/map-value.png" imageMaxHeight={150} />
          <MapContainer id={2} bgColorId={2} onClick={showDetail} title={`Community`} imageSrc="/images/map-community.png" imageMaxHeight={150} />
        </div>
        <div className="w-full space-y-3 mb-3 md:w-6/12 md:grid md:grid-cols-2 md:gap-x-3">
          <MapContainer id={3} bgColorId={1} col={2} onClick={showDetail} title={`MoreHope World`} imageSrc="/images/map-morehope-world.png" imageMaxHeight={150} />
          <MapContainer id={4} bgColorId={3} onClick={showDetail}title={`MoreHope Spot`} imageSrc="/images/map-morehope-spot.png" imageMaxHeight={150} />
          <MapContainer id={5} bgColorId={2} onClick={showDetail} title={`The Program`} imageSrc="/images/map-theprogram.png" imageMaxHeight={150} />

          {/* <div className="grid grid-cols-1 md:grid-cols-2 md:space-x-3">
          </div> */}
        </div>
        <div className="w-full space-y-3 md:w-3/12">
          <MapContainer id={6} bgColorId={2} onClick={showDetail}title={`The Idea`} imageSrc="/images/map-theidea.png" imageMaxHeight={150} />
          <MapContainer id={7} bgColorId={3} onClick={showDetail} title={`Metaverse`} imageSrc="/images/map-metaverse.png" imageMaxHeight={150} />
        </div>
      </div>
    </Layout>
  )
}

const SectionTitle = ({ title }) => (
  <div className="font-bold uppercase">{title}</div>
)

const SectionImage = ({ src, maxHeight }) => (
  <img src={src} style={{maxHeight: maxHeight || 250 }} />
)

const generateBackgroundColor = (id) => {
  switch (id) {
    case 1:
      return "#A6CDC6"
    case 2:
      return "#63B6A5"
    case 3:
      return "#007461"
    default:
      return "#A6CDC6"
  }
}

const MapContainer = ({ id, bgColorId = 1, col = 0, title, imageSrc, imageMaxHeight = 100, onClick }) => (
  <div className={`border shadow-lg rounded-lg p-5 cursor-pointer ${col ? "col-span-2" : "col-span-1"}`} style={{backgroundColor: generateBackgroundColor(bgColorId)}} onClick={() => onClick(id)}>
    <div className="font-bold uppercase text-2xl">{id}</div>
    <div className="flex justify-around items-center">
      <SectionTitle title={title} />
      <SectionImage src={imageSrc} maxHeight={imageMaxHeight} />
    </div>
  </div>
)