import Layout from "components/layout";

export default function ({}) {
  return (
    <Layout title="More News" currentPage="more-news">
      <div className="container mx-auto md:flex md:space-x-5">
        <div className="md:w-2/6">
          <img src="/images/more-news.png" />
        </div>
        <div className="md:w-4/6">
          <a className="twitter-timeline" href="https://twitter.com/madebyais?ref_src=twsrc%5Etfw">Tweets by @morehopeindonesia</a>
          <script async src="https://platform.twitter.com/widgets.js"></script>
        </div>
      </div>
    </Layout>
  )
}